
  <!-- Footer -->
  <footer class="py-5">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; <?php echo $site_name; ?></p>
      <?php if ($site_don == 1) { ?>
      <p class="m-0 text-center text-white">Online store works only with <i class="fab fa-paypal paypal"></i> (PayPal)</p>
      <?php } else {} ?>
    </div>
    <!-- /.container -->

    <!-- Custom JS -->
    <script>
      //shopSearch start
      function shopSearch() {
      // Declare Variables
      var input, filter, table, tr, td, i;
      input = document.getElementById("shopSearch");
      filter = input.value.toUpperCase();
      table = document.getElementById("shopTable");
      var rows = table.getElementsByTagName("tr");

        // Begin Search
        for (i = 0; i < rows.length; i++) {
          var cells = rows[i].getElementsByTagName("td");
          var j;
          var rowContainsFilter = false;
        for (j = 0; j < cells.length; j++) {
          if (cells[j]) {
            if (cells[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
              rowContainsFilter = true;
              continue;
            }
          }
        }

        // Change elements display style
        if (! rowContainsFilter) {
          rows[i].style.display = "none";
        } else {
          rows[i].style.display = "";
        }
      }
    }
    //shopSearch end
</script>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo $custdir; ?>/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo $custdir; ?>/js/main.js"></script>
  <script src="<?php echo $custdir; ?>/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>