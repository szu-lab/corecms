<?php /** @noinspection HtmlUnknownTarget */
include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title text-center"><i class="fad fa-exclamation-triangle"></i> 404 NOT FOUND</h2>
                        <p class="text-center"><img src="https://wow.zamimg.com/modelviewer/live/webthumbs/npc/113/5233.png" alt="spirt healer" /><br />
                        <a href="<?php echo $custdir ?>/" class="btn btn-outline-warning">Go to home</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>